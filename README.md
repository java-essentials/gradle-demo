# gradle-demo

![logo](./img/gradle-dark-green-primary.png)

## Getting started
[Gradle Sample](https://docs.gradle.org/current/samples/sample_building_java_applications.html)

[Spring Gradle Demo](https://spring.io/guides/gs/gradle/)

[Gradle User Guide](https://docs.gradle.org/current/userguide/declaring_dependencies.html)